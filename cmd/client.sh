cmd_client_help() {
    cat <<_EOF
    ds client add <client>
    ds client ls
    ds client share <client>
    ds client revoke <client>

_EOF
}

cmd_client() {
    local cmd=$1
    shift
    case $cmd in
        add|ls|share|revoke)
            _client_$cmd "$@"
            ;;
        *) _client_fail ;;
    esac
}

_client_fail() {
    fail "Usage:\n$(cmd_client_help)"
}

_client_add() {
    local client=$1
    [[ -z $client ]] && _client_fail

    ds shell \
       MENU_OPTION='1' \
       CLIENT="$client" \
       PASS='1' \
       openvpn-install.sh > /dev/null
    
    mkdir -p data/clients/
    ds exec cp /root/$client.ovpn /host/data/clients/
    ls data/clients/$client.*
}

_client_ls() {
    tail -n +2 config/easy-rsa/pki/index.txt \
        | sed 's/\t.*=/ /' \
        | sed -e 's/^V/+/' -e 's/^R/-/'
}

_client_share() {
    local client=$1
    [[ -z $client ]] && _client_fail

    ds exec onionshare /host/data/clients/$client.ovpn
}

_client_revoke() {
    local client=$1
    [[ -z $client ]] && _client_fail

    ds inject revoke-client.sh $client > /dev/null
    rm -f data/clients/$client.ovpn
}
