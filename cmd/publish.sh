cmd_publish_help() {
    cat <<_EOF
    ds publish <config-file.ovpn>

_EOF
}

cmd_publish() {
    local config_file=$1
    [[ -n $config_file ]] \
        || fail "Usage:\n$(cmd_publish_help)\n"
    if [[ ! -f $config_file ]]; then
        fail "Configuration file '$config_file' not found

Usage:
$(cmd_publish_help)

Available configuration files:

$(ls data/clients/*)
"
    fi

    local client=$(basename ${config_file//.ovpn})
    local key=$(tr -cd '[:alnum:]' < /dev/urandom | head -c30)
    cp -f $config_file data/published/$client-$key.ovpn
    tree ./data/published/
    echo
    echo "wget -O $client.ovpn https://$DOMAIN/clients/$client-$key.ovpn"
    echo
}

