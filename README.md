# OpenVPN container

## Installation

- First install `ds`: https://gitlab.com/docker-scripts/ds#installation

- Then get the scripts: `ds pull openvpn`

- Create a directory for the container: `ds init openvpn @openvpn`

- Fix the settings: `cd /var/ds/openvpn/ ; vim settings.sh`

- Make the container: `ds make`

## Usage

1. Add a couple of clients:

   ```
   ds client add client1
   ds client add client2
   ds client add client3
   ds client ls
   ls data/clients/
   ```

2. Revoke one of them:

   ```
   ds client revoke client1
   ds client ls
   ls data/clients/
   ```

3. Share a client config file:

   ```
   ds client share client2
   ```

   Use a Tor Browser on the client side to download the configuration
   file.

4. Publish a config file through apache2:

   ```
   ls data/clients/
   ds publish data/clients/client3.ovpn
   ls data/published/
   ```

   On the client side use a command like this to get the config file:
   
   ```
   wget -O client3.ovpn \
       https://ovpn.example.org/clients/client3-5VeIKKzsz7Jj4sTE23ddgn3VIBZQUb.ovpn
   ```

   Then, on the server, remove the published file:

   ```
   rm data/published/*
   ```

   **NB:** This works only if you have uncommented `DOMAIN` on
   `settings.sh`.

5. Test the configuration file (on the client side):

   ```
   apt install openvpn
   openvpn client3.ovpn
   ```

6. Start the VPN connection as a service:

   ```
   mv client3.ovpn /etc/openvpn/client3.conf
   systemctl enable openvpn@client3
   systemctl start openvpn@client3
   ```

7. To monitor the connected clients open:
   https://ovpn.example.org/monitor/

   **NB:** This works only if you have uncommented `DOMAIN` on
   `settings.sh`.

## Enable the Bridge Mode

To enable the Bridge Mode, uncomment the line `BRIDGE_MODE=yes` on
`settings.sh` and then rebuild the container (with `ds make`).
To disable it, comment it out and remake.

When the Bridge Mode is enabled the clients will be connected to
the same network as the docker container (which is the internal docker
network that is maintained by docker-scripts).

If `CLIENT_TO_CLIENT` is uncommented as well, then the clients will
also be able to see (ping) each-other.

## Assign fixed IPs to some clients

To assign the fixed IP `10.8.0.50` to `client1`, create the file
`config/ccd/client1` with the line:

```
ifconfig-push 10.8.0.50 255.255.255.0
```

Then restart the server (`ds restart`) and reconnect the clients.

Note that the IP **.50** that we are using as an example here should
be outside the range of `OVPN_IP_POOL` on `settings.sh` (which by
default is `"100:250"`), otherwise an IP clash may happen with another
client (that gets an IP from the pool) and this may cause problems for
both clients.

## Other commands

```
ds stop
ds start
ds shell
ds help
```
