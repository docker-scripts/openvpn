#!/bin/bash

source /host/settings.sh

### enable openvpn management interface on localhost
cat <<EOF >> /etc/openvpn/server.conf
# Enable OpenVPN Management Interface on localhost using TCP port 5555
management 127.0.0.1 5555
EOF

### create an apache2 config
cat <<EOF >> /etc/apache2/conf-available/monitor.conf
RedirectMatch ^/$ https://$DOMAIN/monitor/

<Directory /var/www/html/monitor>
    Options +ExecCGI
    AddHandler cgi-script .py
    DirectoryIndex openvpn-monitor.py
    AllowOverride All
</Directory>
EOF

### setup basic authentication
cat <<EOF > /var/www/html/monitor/.htaccess
AuthType Basic
AuthName "OpenVPN Monitor"
AuthUserFile /etc/apache2/.htpasswd
Require valid-user
EOF

### add a user and password
htpasswd -cb /etc/apache2/.htpasswd $ADMIN $PASS

### enable the config and restart apache2
a2enconf monitor
a2enmod cgi
systemctl restart apache2

### customize the config file of openvpn-monitor
sed -i /var/www/html/monitor/openvpn-monitor.conf \
    -e '/site=/ c site=' \
    -e '/maps=/ c maps=False' \
    -e '/name=/ c name=VPN Clients'
