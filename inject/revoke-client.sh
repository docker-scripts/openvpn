#!/bin/bash

client=$1
[[ -z $client ]] && exit 1

cd /etc/openvpn/easy-rsa/ || exit 1

[[ -z $(tail -n +2 pki/index.txt | grep "^V" | grep "CN=$client") ]] && exit 1

# revoke and remove
./easyrsa --batch revoke "$client"
sed -i "/^$client,.*/d" ../ipp.txt
rm -f /root/$client.ovpn

# generate a new crl.pem
EASYRSA_CRL_DAYS=3650 ./easyrsa gen-crl
rm -f ../crl.pem
cp pki/crl.pem ../
chmod 644 ../crl.pem
